FROM debian:bookworm-slim

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

ARG TARGETOS
ARG TARGETARCH

# renovate: datasource=github-releases depName=fluxcd/flux2 VersionTemplate=v
ENV FLUX_VERSION "2.3.0"

# hadolint ignore=DL3008, DL3015
RUN apt-get update \
    && apt-get install -y wget apt-transport-https gnupg lsb-release libguestfs-tools \
    && wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | gpg --dearmor | tee /usr/share/keyrings/trivy.gpg > /dev/null \
    && echo "deb [signed-by=/usr/share/keyrings/trivy.gpg] https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main" | tee -a /etc/apt/sources.list.d/trivy.list \
    && apt-get update \
    && apt-get install -y trivy --no-install-recommends \
    && wget -q --show-progress --progress=bar:force "https://github.com/fluxcd/flux2/releases/download/v${FLUX_VERSION}/flux_${FLUX_VERSION}_${TARGETOS}_${TARGETARCH}.tar.gz" -O - | tar -xzO flux > /usr/local/bin/flux \
    && chmod +x /usr/local/bin/flux \
    && libguestfs-test-tool \
    && trivy -v \
    && flux -v \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
