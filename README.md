# trivy-libguestfs

This Docker image provides a ready-to-use environment with Trivy, Libguestfs and Flux  installed. It can be used for various purposes, such as analyzing and scanning virtual machine (VM) disk images for vulnerabilities.

## Image includes the following tools

1. **trivy**
   - Trivy is a simple and comprehensive vulnerability scanner for containers. It can scan container images and filesystems to detect vulnerabilities and provide detailed reports.

2. **libguestfs**
   - Libguestfs is a set of tools for accessing and modifying VM disk images. It provides a wide range of functionality for managing VM disk images, including creating, modifying, and inspecting disk images.

3. **Flux CLI**
    - Flux CLI is a command-line interface tool for managing and deploying applications using Flux, a GitOps tool for Kubernetes. It allows users to interact with Flux and perform operations such as syncing, releasing, and monitoring applications deployed on Kubernetes clusters.
